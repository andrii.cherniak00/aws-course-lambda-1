package com.aws.course;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Lambda implements RequestHandler<Map<String, String>, String> {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String queueUrl = "https://sqs.us-west-2.amazonaws.com/968878561243/task-8-aws-app-uploads-notification-queue";
    String topicArn = "arn:aws:sns:us-west-2:968878561243:task-8-aws-app-uploads-notification-topic";
    AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
    AmazonSNS sns = AmazonSNSClientBuilder.defaultClient();


    @Override
    public String handleRequest(Map<String, String> event, Context context) {

        LambdaLogger logger = context.getLogger();
        String response = "200 OK";
        // log execution details
        logger.log("ENVIRONMENT VARIABLES: " + gson.toJson(System.getenv()));
        logger.log("CONTEXT: " + gson.toJson(context));
        // process event
        logger.log("EVENT: " + gson.toJson(event));
        logger.log("EVENT TYPE: " + event.getClass() + "\n");

        List<Message> messages = getMessages();
        logger.log("SQS # of messages" + messages.size() + "\n");
        logger.log("SQS MESSAGES" + gson.toJson(messages.stream().map(Message::getBody).collect(Collectors.toList())));

        var publishResult = sendMessages(messages);
        logger.log("SNS RESPONSE" + gson.toJson(publishResult));

        return response;
    }

    public List<Message> getMessages() {
        List<Message> messages = new ArrayList<>();
        List<Message> list;
        var receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        receiveMessageRequest.withWaitTimeSeconds(3);

        while (!( list = sqs.receiveMessage(receiveMessageRequest).getMessages()).isEmpty()) {
            messages.addAll(list);
            deleteMessages(list);
        }

        return messages;
    }

    public PublishResult sendMessages(List<Message> messages) {
        List<String> clearMessages = messages.stream().map(Message::getBody).collect(Collectors.toList());
        var publishRequest = new PublishRequest(topicArn, gson.toJson(clearMessages));
        return sns.publish(publishRequest);
    }

    public boolean deleteMessages(List<Message> messages) {
        if (!messages.isEmpty()) {

            var entries = new ArrayList<DeleteMessageBatchRequestEntry>();
            for (Message message : messages) {
                var receiptHandle = message.getReceiptHandle();
                entries.add(new DeleteMessageBatchRequestEntry(message.getMessageId(), receiptHandle));
            }
            var deleteMessageBatchRequest = new DeleteMessageBatchRequest(queueUrl, entries);
            var response = sqs.deleteMessageBatch(deleteMessageBatchRequest);

            if (response.getSuccessful().size() == messages.size()) {
                return true;
            }
        }
        return false;
    }
}
